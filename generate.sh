#!/bin/bash


TMP_DIR="./tmp"


source ./env/bin/activate

time python ./audiovideo-face/src/generate.py \
        --target-movie "./resources/generate_video/test.mp4" \
        --target-audio "./resources/generate_video/test.wav" \
        --result-movie "./resources/generate_video/result.mp4" \
        --audvid-model  "./audiovideo-face/models/audvid/audvid_model.pth" \
        --teeth-proxy-model "./audiovideo-face/models/teeth_proxy/teeth_proxy_model.pth" \
        --motion-gain "1" \
        --tmp-dir $TMP_DIR

deactivate

# rm -r $TMP_DIR