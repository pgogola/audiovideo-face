#!/bin/bash

IN_FILE=$1
OUT_FILE=$2

ffmpeg -y -i "$IN_FILE" -ab 3000k -ar 16000 -ac 1 "$OUT_FILE"
