#!/bin/bash

# DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )/"
DLIB_DIR="dlib-19.19/"
EOS_DIR="eos/"
PARENT_DIR="../"

ENV="env/bin/activate"
CUDA_TOOLKIT_ROOT_DIR="/usr/local/cuda-10.2/" # specify your cuda directory
CUDA_NVCC_EXECUTABLE=${CUDA_TOOLKIT_ROOT_DIR}bin/nvcc
CUDA_INCLUDE_DIRS="TMP"
CUDA_CUDART_LIBRARY=${CUDA_TOOLKIT_ROOT_DIR}lib64/libcudart.so