#!/bin/bash

IN_FILE=$1
OUT_FILE=$2

START_SEC=$3
STOP_SEC=$4

ffmpeg -y -ss "$START_SEC" -i "$IN_FILE" -c copy -t "$STOP_SEC" "$OUT_FILE"