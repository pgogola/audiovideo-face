#!/bin/bash

source ./paths.sh

echo "Intalling EOS library"
source ./env/bin/activate
cd ./dlib-19.19
python ./setup.py install
cd ..
deactivate