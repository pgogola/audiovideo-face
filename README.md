# AudioVideo-Face

## Description

Lip-sync face generator from audio.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

1. Python >= 3.6 (tested with Python 3.6.8)

2. pip >= 20.1.1 (tested with pip 20.1.1)

### Informations

DLib library may require to install libraries:

```bash
sudo apt install libx11-dev
sudo apt install libopenblas-dev
sudo apt install liblapack-dev
```

To run Pytorch and Tensorflow learning frameworks on GPU install *CUDA Toolkit*.

### Installing

Installation can be done with ```install.sh``` script. It performs compilation of *eos* and *dlib* libraries and installs all required libraries inside local Python environment.

Manual *eos* library installation: [eos library README.md](./eos/README.md)

Manual *dlib* library installation: [dlib library README.md](./dlib-19.19/README.md)

### Models

To download and unpack models used by libraries and third part software run ```download_models.sh``` script which downloads *libs_models.tar.xz*
file and extract it into
*audiovideo-face/models/* directory.

### Running the program

#### Training model

To train system prepare movies with talking person (only one person in the frame) and place them in one directory. Movies have to have the same frame rate (for example 24 or 30 fps). Then use it as command line parameter ```--movies-dir``` for ```audiovideo-face/src/train.py``` script.
Other required arguments are:

* ```--audvid-model``` - a place where AudVidFace model will be saved,
* ```--teeth-proxy-model``` -- a place where *pip2pix* teeth-proxy model will be saved.

Example run script is ```train.sh```. For more available parameters run

```bash
python audiovideo-face/src/train.py --help
```

#### Generating new video with changed audio and lips movement

To generate no video call ```audiovideo-face/src/generate.py``` script. Required parameters are:

* ```--target-movie``` - input movie which will be transformed
* ```--target-audio``` - input audio which will be used to generate lips movement in *target-video*
* ```--result-movie``` - a place where result video will be saved
* ```--audvid-model``` - learned AudVidFace model
* ```--teeth-proxy-model``` - learned *pix2pix* teeth-proxy model

Example run script is ```generate.sh```.  For more available parameters run

```bash
python audiovideo-face/src/generate.py --help
```

## Usefull links

1. [https://www.learnopencv.com/face-detection-opencv-dlib-and-deep-learning-c-python/](https://www.learnopencv.com/face-detection-opencv-dlib-and-deep-learning-c-python/)
2. [https://github.com/davisking/dlib-models](https://github.com/davisking/dlib-models)

## Acknowledgments

1. [eos: A lightweight header-only 3D Morphable Face Model fitting library in modern C++11/14.](https://github.com/patrikhuber/eos)

2. [CycleGAN and pix2pix in PyTorch](https://github.com/junyanz/pytorch-CycleGAN-and-pix2pix)

3. [Project DeepSpeech](https://github.com/mozilla/DeepSpeech)

4. [dlib C++ library](https://github.com/davisking/dlib)