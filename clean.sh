#!/bin/bash

# Remove eos build directories
echo "Removing eos build directories"
rm -r eos/build eos/dist eos/eos_py.egg-info

# Remove dlib-19.19 build directories
echo "Removing dlib build directories"
rm -r dlib-19.19/build dlib-19.19/dist dlib-19.19/dlib.egg-info

# Remove env directories
echo "Removing local python environment build directories"
rm -r env/
