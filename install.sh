#!/bin/bash

SCRIPTS_DIR="scripts/"
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )/"
echo $DIR

source ./scripts/make_env.sh
source ./scripts/build_dlib.sh
source ./scripts/build_eos.sh
source ./scripts/install_python_dependencies.sh