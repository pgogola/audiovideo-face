#!/bin/bash

TMP_DIR="./tmp"
result_teeth_dir=$TMP_DIR"/data"

source ./env/bin/activate

time python ./audiovideo-face/src/train.py \
        --movies-dir "./resources/train_video" \
        --audvid-model "./audiovideo-face/models/audvid/audvid_model.pth" \
        --audvid-epochs 100 \
        --audvid-epochs-decay 100 \
        --audvid-batch 16 \
        --tmp-dir $TMP_DIR

time python ./pytorch-CycleGAN-and-pix2pix/train.py \
        --dataroot $result_teeth_dir \
        --name "teeth_pix2pix" \
        --model "pix2pix" \
        --direction "AtoB" \
        --save_by_iter \
        --save_epoch_freq 1 \
        --lambda_L1 "10" \
        --dataset_mode "aligned" \
        --norm "batch" \
        --pool_size 0 \
        --n_epochs "10" \
        --n_epochs_decay "10" \
        --teeth-proxy-model "./audiovideo-face/models/teeth_proxy/teeth_proxy_model.pth"


deactivate