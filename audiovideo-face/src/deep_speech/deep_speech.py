from tensorflow.python.ops import gen_audio_ops as contrib_audio
import tensorflow as tf
import numpy as np
import os
n_input = 26
n_context = 9
n_steps = 16
n_cell_dim = 2048


class DeepSpeech(object):
    def __init__(self, window_length=32,
                 window_stride=20,
                 predicted_characters=29,
                 model_path="resources/models/deepspeech/output_graph.pb"):
        my_path = os.path.abspath(os.path.dirname(__file__))
        models_path = os.path.join(my_path, "../../models/deep_speech")
        self.window_length = window_length
        self.window_stride = window_stride
        self.predicted_characters = predicted_characters
        self.model_path = models_path + "/deep_speech_model.pb"  # model_path

    def load_model(self):
        with tf.io.gfile.GFile(self.model_path, "rb") as f:
            graph_def = tf.compat.v1.GraphDef()
            graph_def.ParseFromString(f.read())

        self.graph = tf.Graph()
        with self.graph.as_default() as _:
            tf.import_graph_def(graph_def, name="prefix")

    def samples_to_mfccs(self, samples, sample_rate):
        # 16000 = default sample rate
        # 32 = default feature extraction audio window length in milliseconds
        audio_window_samples = 16000 * (self.window_length / 1000)
        # 20 = default feature extraction window step length in milliseconds
        audio_step_samples = 16000 * (self.window_stride / 1000)
        spectrogram = contrib_audio.audio_spectrogram(samples,
                                                      window_size=audio_window_samples,
                                                      stride=audio_step_samples,
                                                      magnitude_squared=True)

        mfccs = contrib_audio.mfcc(
            spectrogram, sample_rate, dct_coefficient_count=n_input)
        mfccs = tf.reshape(mfccs, [-1, n_input])

        return mfccs, tf.shape(input=mfccs)[0]

    def audiofile_to_features(self, wav_filename):
        samples = tf.io.read_file(wav_filename)
        decoded = contrib_audio.decode_wav(samples, desired_channels=1)
        features, features_len = self.samples_to_mfccs(
            decoded.audio, decoded.sample_rate)
        return features, features_len

    def create_overlapping_windows(self, batch_x):
        batch_size = tf.shape(input=batch_x)[0]
        window_width = 2 * n_context + 1
        num_channels = n_input

        # Create a constant convolution filter using an identity matrix, so that the
        # convolution returns patches of the input tensor as is, and we can create
        # overlapping windows over the MFCCs.
        eye_filter = tf.constant(np.eye(window_width * num_channels)
                                 .reshape(window_width, num_channels,
                                          window_width * num_channels), tf.float32)  # pylint: disable=bad-continuation

        # Create overlapping windows
        batch_x = tf.nn.conv1d(
            input=batch_x, filters=eye_filter, stride=1, padding='SAME')

        # Remove dummy depth dimension and reshape into [batch_size, n_windows, window_width, n_input]
        batch_x = tf.reshape(
            batch_x, [batch_size, -1, window_width, num_channels])

        return batch_x

    def run_inference(self, audio_path):
        with tf.compat.v1.Session(graph=self.graph) as session:

            features, features_len = self.audiofile_to_features(audio_path)
            previous_state_c = np.zeros([1, n_cell_dim])
            previous_state_h = np.zeros([1, n_cell_dim])

            # Add batch dimension
            features = tf.expand_dims(features, 0)
            features_len = tf.expand_dims(features_len, 0)

            # Evaluate
            features = self.create_overlapping_windows(
                features).eval(session=session)
            features_len = features_len.eval(session=session)

            inputs = {'input': self.graph.get_tensor_by_name('prefix/input_node:0'),
                      'previous_state_c': self.graph.get_tensor_by_name('prefix/previous_state_c:0'),
                      'previous_state_h': self.graph.get_tensor_by_name('prefix/previous_state_h:0'),
                      'input_lengths': self.graph.get_tensor_by_name('prefix/input_lengths:0')}
            outputs = {'outputs': self.graph.get_tensor_by_name('prefix/logits:0'),
                       'new_state_c': self.graph.get_tensor_by_name('prefix/new_state_c:0'),
                       'new_state_h': self.graph.get_tensor_by_name('prefix/new_state_h:0')}

            logits = np.empty([0, 1, self.predicted_characters])

            # the frozen model only accepts input split to 16 step chunks,
            # if the inference was run from checkpoint instead (as in single inference in deepspeech script),
            # this loop wouldn't be needed
            for i in range(0, features_len[0], n_steps):
                chunk = features[:, i:i + n_steps, :, :]
                chunk_length = chunk.shape[1]
                # pad with zeros if not enough steps (len(features) % FLAGS.n_steps != 0)
                if chunk_length < n_steps:
                    chunk = np.pad(chunk,
                                   (
                                       (0, 0),
                                       (0, n_steps - chunk_length),
                                       (0, 0),
                                       (0, 0)
                                   ),
                                   mode='constant',
                                   constant_values=0)

                # need to update the states with each loop iteration
                logits_step, previous_state_c, previous_state_h = session.run(
                    [outputs['outputs'], outputs['new_state_c'], outputs['new_state_h']], feed_dict={
                        inputs['input']: chunk,
                        inputs['input_lengths']: [chunk_length],
                        inputs['previous_state_c']: previous_state_c,
                        inputs['previous_state_h']: previous_state_h,
                    })
                logits = np.concatenate((logits, logits_step))

            logits = np.squeeze(logits)
        return logits


if __name__ == '__main__':
    m = DeepSpeech()
    m.load_model()
    logits = m.run_inference(
        audio_path="./resources/audio1.wav")

    import pickle
    with open("./resources/d.pickle", 'br') as f:
        d = pickle.load(f)

    print(d-logits)

    import matplotlib.pyplot as plt
    plt.imshow(logits)
    plt.show()
    print(logits.shape)

    # logits = run_inference(model_path="resources/models/deepspeech/output_graph.pb",
    #                        audio_path="resources/eng_res/audio1.wav",
    #                        predicted_characters=29)
    print(logits)

    # import pickle
    # with open('audio.pkl', 'wb') as f:
    #     pickle.dump(logits, f)
