import torchvision.transforms as transforms
from PIL import Image
import pickle
import torch
import utils.logger as log
import cv2
import numpy as np
import glfw
import math
import argparse
import os
import subprocess
import time
from scipy import signal
from pathlib import Path
from utils.audio import extract_audio
from utils.video import (
    get_video_details,
    generate_video_from_frames,
    generate_video_from_frames_directory,
    merge_video_audio
)
from utils.helpers import *
from utils.time_count import *
from deep_speech.deep_speech import DeepSpeech
from face_detection.face_detector import FaceDetector
from face_detection.landmarks_detector import LandmarksDetector
from face_3d.face_renderer import FaceRenderer, prepare_window
from face_3d.morphable_face_model import (
    MorphableFaceModel, transform_morph_model_to_opengl_data
)
import audvid_face.audvid_face as avf
from audvid_face.audvid_face import AudVid
from OpenGL.GL import *
from OpenGL.GLU import *
from imutils import face_utils
from scipy.special import softmax
from sklearn.preprocessing import StandardScaler
from scipy.signal import savgol_filter


def make_data_per_frame(
    audio_logits,
    video_fps,
    audio_stride=10,
    audio_width=16
):
    data = []
    video_stride = 1000 / video_fps
    start = 0
    i = 0
    while start < len(audio_logits):
        start = int(video_stride * i / audio_stride)
        i += 1
        logits = audio_logits[start:start + audio_width]
        if len(logits) < audio_width:
            continue
        data.append(np.expand_dims(logits, axis=0))
    return data


def generate_frames(
    source_video_file,
    coefficients,
    face_detector,
    landmarks_detector,
    face_3d_model,
    teeth_proxy_model_path,
    opt,
    tmp_dir=None
):
    cap = cv2.VideoCapture(source_video_file)
    _, image = cap.read()
    window = prepare_window(
        width=image.shape[1], height=image.shape[0])
    cap.set(cv2.CAP_PROP_POS_FRAMES, 0)

    face_renderer = FaceRenderer(teeth_proxy_model_path)
    face_renderer.prepare_shaders()
    face_renderer.prepare_buffers()

    frames = []

    len_coefficients = len(coefficients)
    # win = signal.hann(9)
    win_shape = np.array([1, 1, 1, 1, 1])
    win_shape = np.expand_dims(win_shape, axis=1)
    win_blendshape = np.array([0.25, 0.60, 1.00, 0.60, 0.25])
    win_blendshape = np.expand_dims(win_blendshape, axis=1)
    filtered = np.hstack(
        (
            signal.convolve(
                coefficients[:, :63], win_shape, mode='same'
            ) / sum(win_shape),
            opt.motion_gain*savgol_filter(coefficients[:, 63:], 7, 2, axis=0)
        )
    )
    coefficients = filtered
    lcoef = 0

    target_texture = None

    if not opt.texture_image is None:
        image = cv2.imread(opt.texture_image)
        rects = face_detector.get_face_rects(image)
        if 0 != len(rects):
            landmark_points = landmarks_detector.get_landmarks(image, rects[0])
            mesh, rendering_param, shape_coefficients, blendshape_coefficients = face_3d_model.fit_face_3d(
                landmark_points, image.shape[1], image.shape[0])
            to_texture = np.zeros_like(
                cv2.cvtColor(image, cv2.COLOR_BGR2GRAY))
            mask_convexhull = np.zeros_like(to_texture)
            mask_convexhull_l = cv2.convexHull(landmark_points)
            to_texture = cv2.fillConvexPoly(
                mask_convexhull, mask_convexhull_l, 255)
            to_texture = cv2.bitwise_and(image, image, mask=to_texture)
            target_texture = face_3d_model.get_texture(
                mesh, rendering_param, to_texture)
            target_texture = np.transpose(target_texture, (1, 0, 2))

    while lcoef < len_coefficients:
        retval, image = cap.read()
        if retval is False:
            break
        rects = face_detector.get_face_rects(image)
        if 0 != len(rects):
            landmark_points = landmarks_detector.get_landmarks(image, rects[0])
            mesh, rendering_param, shape_coefficients, blendshape_coefficients = face_3d_model.fit_face_3d(
                landmark_points, image.shape[1], image.shape[0])
        else:
            continue

        if lcoef % 100 == 0:
            log.log_info(
                f"Generating frames from coefficient list: {lcoef}/{len_coefficients}")
        coeff = coefficients[lcoef]

        to_texture = np.zeros_like(
            cv2.cvtColor(image, cv2.COLOR_BGR2GRAY))
        mask_convexhull = np.zeros_like(to_texture)
        mask_convexhull_l = cv2.convexHull(landmark_points)
        to_texture = cv2.fillConvexPoly(
            mask_convexhull, mask_convexhull_l, 255)
        to_texture = cv2.bitwise_and(image, image, mask=to_texture)

        if target_texture is None:
            target_texture = face_3d_model.get_texture(
                mesh, rendering_param, to_texture)
            target_texture = np.transpose(target_texture, (1, 0, 2))

        mesh = face_3d_model.generate_mesh(
            coeff[:63].tolist(),  coeff.tolist()[63:], []
        )
        h, w = image.shape[:2]
        target_texture_mask = np.ones((h, w), dtype=np.uint8)*255
        vertices, indices = transform_morph_model_to_opengl_data(
            mesh, rendering_param)

        glfw.poll_events()
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
        face_renderer.update_model(vertices, indices,
                                   image=target_texture_mask)
        face_renderer.render_face_3d()
        glfw.swap_buffers(window)
        result_mask = cv2.cvtColor(face_renderer.render_face_image(
            w, h, landmark_points, None), cv2.COLOR_RGB2GRAY)
        cv2.floodFill(result_mask, np.zeros(
            (h + 2, w + 2), np.uint8), (0, 0), 255)
        mouth_area = np.argwhere(result_mask == 0)
        teeth_render = False
        if len(mouth_area) != 0:
            m_max_x = np.max(mouth_area[:, 0])+1
            m_max_y = np.max(mouth_area[:, 1])+1
            m_min_x = np.min(mouth_area[:, 0])-1
            m_min_y = np.min(mouth_area[:, 1])-1
            max_width = max(m_max_x-m_min_x, m_max_y-m_min_y)
            middle_x = (m_max_x+m_min_x)//2
            middle_y = (m_max_y+m_min_y)//2
            m_max_x = middle_x + max_width//2
            m_max_y = middle_y + max_width//2
            m_min_x = middle_x - max_width//2
            m_min_y = middle_y - max_width//2
            teeth_render = True
        time.sleep(0.01)

        glfw.poll_events()
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
        face_renderer.update_model(vertices, indices,
                                   image=target_texture)
        face_renderer.render_face_3d()
        glfw.swap_buffers(window)
        result = face_renderer.render_face_image(
            image.shape[1], image.shape[0], landmark_points, image)
        if teeth_render:
            result = face_renderer.render_teeth_proxy(
                result, face_detector, (m_max_x, m_max_y, m_min_x, m_min_y))
        cv2.imshow("result", result)
        cv2.waitKey(1)
        if tmp_dir is None:
            frames.append(result)
        else:
            frame_path = f"{tmp_dir}/frames/{lcoef}.png"
            cv2.imwrite(frame_path, result)
            frames.append(frame_path)
        lcoef += 1
    return frames


def replace_audio_in_video(
        source_video_file,
        source_audio_file,
        audio_logits,
        target_video_file,
        audvid_face,
        teeth_proxy_model,
        face_detector,
        landmarks_detector,
        face_3d_model,
        tmp_dir,
        opt
):

    data_prepare_start_time = get_time()
    video_fps, frames_count = get_video_details(source_video_file)
    audvid_face.eval()
    device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
    audvid_face.to(device)
    data_audio = avf.prepare_data_audio_to_prediction(
        audio_logits=audio_logits,
        video_fps=video_fps,
        audio_fps=50
    )
    data_prepare_stop_time = get_time()
    log.log_info(
        "Prepare data elapsed time: " +
        get_elapsed_time(data_prepare_start_time,
                         data_prepare_stop_time)
    )
    data_audio = avf.prepare_logits_to_prediction(data_audio).to(device)
    coefficients, _ = audvid_face(data_audio)
    coefficients = coefficients.cpu().detach().numpy()
    generate_frames_start = get_time()
    frames = generate_frames(
        source_video_file,
        coefficients,
        face_detector,
        landmarks_detector,
        face_3d_model,
        teeth_proxy_model,
        opt,
        tmp_dir=None
    )
    generate_frames_stop = get_time()
    log.log_info(
        "Generate frames elapsed time: " +
        get_elapsed_time(generate_frames_start,
                         generate_frames_stop)
    )
    target_video = os.path.join(tmp_dir, "raw_video.avi")
    if type(frames[0]) is str:
        generate_video_from_frames_directory(frames, video_fps, target_video)
    else:
        generate_video_from_frames(frames, video_fps, target_video)
    merge_vid_audio_start = get_time()
    merge_video_audio(
        target_video, source_audio_file, target_video_file)
    merge_vid_audio_stop = get_time()
    log.log_info(
        "Mixing video with audio elapsed time: " +
        get_elapsed_time(merge_vid_audio_start,
                         merge_vid_audio_stop)
    )


def generate_video(
    audvid_face,
    teeth_proxy_model,
    input_video,
    input_audio,
    result_movie,
    tmp_dir,
    opt
):
    deep_speech = DeepSpeech()
    deep_speech.load_model()

    audio_logits_prepare_start = get_time()
    audio_logits = deep_speech.run_inference(input_audio)
    audio_logits = softmax(audio_logits)
    audio_logits = StandardScaler().fit_transform(audio_logits)*10
    audio_logits_prepare_stop = get_time()
    log.log_info(
        "Prepare audio logits elapsed time: " +
        get_elapsed_time(audio_logits_prepare_start,
                         audio_logits_prepare_stop)
    )

    face_detector = FaceDetector(gpu=False)
    landmarks_detector = LandmarksDetector()
    face_3d_model = MorphableFaceModel()
    replace_audio_in_video(input_video, input_audio,
                           audio_logits, result_movie,
                           audvid_face, teeth_proxy_model,
                           face_detector, landmarks_detector,
                           face_3d_model, tmp_dir, opt)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        formatter_class=argparse.RawTextHelpFormatter)
    required_args = parser.add_argument_group('required arguments')
    required_args.add_argument("--target-movie", type=str, dest="target_movie",
                               action="store", required=True,
                               help="Path to movie used to generate changed lips movement.")
    required_args.add_argument("--target-audio", type=str, dest="target_audio",
                               action="store", required=True,
                               help="Path to movie used to generate changed lips movement.")
    required_args.add_argument("--result-movie", type=str, dest="result_movie",
                               action="store", required=True,
                               help="Path to movie used to generate changed lips movement.")
    required_args.add_argument("--audvid-model", type=str, dest="audvid_model",
                               action="store", required=True,
                               help="AudVid model output name.")
    required_args.add_argument("--teeth-proxy-model", type=str, dest="teeth_proxy_model",
                               action="store", required=True,
                               help="Teeth proxy model output name.")
    required_args.add_argument("--motion-gain", type=float, dest="motion_gain",
                               action="store", required=False, default=1.0,
                               help="Mouth motion gain.")
    required_args.add_argument("--texture", type=str, dest="texture_image",
                               action="store", required=False, default=None,
                               help="Path to image used as texture.")
    required_args.add_argument("--tmp-dir", type=str, dest="tmp_dir",
                               action="store", required=False, default="./tmp",
                               help="Temporary work directory.")
    args = parser.parse_args()

    Path(args.tmp_dir).mkdir(parents=True, exist_ok=True)
    Path(args.tmp_dir + "/frames").mkdir(parents=True, exist_ok=True)

    audvid_face = avf.load_model(args.audvid_model)
    generate_video(audvid_face, args.teeth_proxy_model, args.target_movie,
                   args.target_audio, args.result_movie, args.tmp_dir, args)
