from moviepy.video.io.ffmpeg_tools import ffmpeg_extract_audio
from moviepy.config import get_setting
from moviepy.tools import subprocess_call


def extract_audio(inputfile, output, bitrate, fps):
    """
    video_file_name : video to extract audio
    output_file_name : output audio file name
    """
    cmd = [get_setting("FFMPEG_BINARY"), "-y", "-i", inputfile, "-ab", "%dk" % bitrate,
           "-ar", "%d" % fps, "-ac", "1", output]
    subprocess_call(cmd)
