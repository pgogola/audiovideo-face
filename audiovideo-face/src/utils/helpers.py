import numpy as np


def normalize_vector(
        org_vector,
        new_min_value,
        new_max_value,
        vector_min_value=None,
        vector_max_value=None
):
    if vector_max_value is None:
        vector_max_value = np.max(org_vector)
    if vector_min_value is None:
        vector_min_value = np.min(org_vector)
    return (new_max_value - new_min_value) / \
        (vector_max_value - vector_min_value) * \
        (org_vector - vector_max_value) + new_max_value
