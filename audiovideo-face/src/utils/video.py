from moviepy.video.io.ffmpeg_tools import (
    ffmpeg_extract_subclip, ffmpeg_merge_video_audio
)
import cv2


def cut_video(
    video_file_name,
    start_time,
    stop_time,
    sample_length=None,
    output_dir=None
):
    """
    video_file_name : path to video which should be cut
    start_time : video start timestamp in tuple format (HH, MM, SS, xxx)
    stop_time : video stop timestamp in tuple format (HH, MM, SS, xxx)
    sample_length : each cut sample length in tuple format (HH, MM, SS, xxx)
    """
    def to_sec(t):
        return 60 * (t[0] * 60 + t[1]) + t[2] + 0.001 * t[3]

    start_time_sec = to_sec(start_time)
    stop_time_sec = to_sec(stop_time)
    if sample_length is None:
        sample_length_sec = stop_time - start_time
    else:
        sample_length_sec = to_sec(sample_length)

    it = 0
    while start_time_sec < stop_time_sec:
        ffmpeg_extract_subclip(
            video_file_name, start_time_sec, start_time_sec + sample_length_sec,
            f"{output_dir}/cut_vid{it}.mp4" if output_dir is not None else None)
        start_time_sec += sample_length_sec
        it += 1


def generate_video_from_frames(frames, fps, output):
    size = (frames[0].shape[1], frames[0].shape[0])
    fourcc = cv2.VideoWriter_fourcc(*'mp4v')
    out = cv2.VideoWriter(output, fourcc, fps, size)

    for frame in frames:
        out.write(frame)
    out.release()


def generate_video_from_frames_directory(frames, fps, output):
    frame = cv2.imread(frames[0])
    height, width, _ = frame.shape
    size = (width, height)
    fourcc = cv2.VideoWriter_fourcc(*'mp4v')
    out = cv2.VideoWriter(output, fourcc, fps, size)

    for frame in frames:
        out.write(cv2.imread(frame))
    out.release()


def merge_video_audio(video, audio, output):
    ffmpeg_merge_video_audio(video, audio, output, acodec='aac')


def get_video_details(video):
    cap = cv2.VideoCapture(video)
    return cap.get(cv2.CAP_PROP_FPS), cap.get(cv2.CAP_PROP_FRAME_COUNT)


if __name__ == "__main__":
    input_video_file = "./resources/video/v1_360p.mp4"
    output_video_dir = "./resources/video/result"

    cut_video(input_video_file, (0, 0, 1, 0), (0, 1, 0, 0),
              (0, 0, 0, 500), output_video_dir)
