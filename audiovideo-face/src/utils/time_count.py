import time


def get_time():
    return time.time()


def get_elapsed_time(start_time, stop_time):
    elapsed_time = stop_time - start_time
    return time.strftime("%H:%M:%S", time.gmtime(elapsed_time))
