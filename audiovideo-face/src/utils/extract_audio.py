from moviepy.video.io.ffmpeg_tools import ffmpeg_extract_audio
import sys
import logger as log


if __name__ == "__main__":
    in_v = str(sys.argv[1])
    out_v = str(sys.argv[2])
    log.log_green(f"Extracting audio from video {in_v} into {out_v}")
    ffmpeg_extract_audio(in_v, out_v, bitrate=3000, fps=16000)
