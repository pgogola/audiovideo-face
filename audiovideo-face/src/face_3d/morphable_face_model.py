import eos
import cv2
import numpy as np
import os.path


class MorphableFaceModel:
    def __init__(self):
        my_path = os.path.abspath(os.path.dirname(__file__))
        models_path = os.path.join(my_path, "../../models/face_3d")
        self.model = eos.morphablemodel.load_model(
            models_path + "/sfm_shape_3448.bin")
        self.blendshapes = eos.morphablemodel.load_blendshapes(
            models_path + "/expression_blendshapes_3448.bin")
        # Create a MorphableModel with expressions
        # from the loaded neutral model and blendshapes:
        self.morphablemodel_with_expressions = \
            eos.morphablemodel.MorphableModel(
                self.model.get_shape_model(), self.blendshapes,
                color_model=eos.morphablemodel.PcaModel(),
                vertex_definitions=None,
                texture_coordinates=self.model.get_texture_coordinates())
        self.landmark_mapper = eos.core.LandmarkMapper(
            models_path + "/ibug_to_sfm.txt"
        )
        self.edge_topology = eos.morphablemodel.load_edge_topology(
            models_path + "/sfm_3448_edge_topology.json"
        )
        self.contour_landmarks = eos.fitting.ContourLandmarks.load(
            models_path + "/ibug_to_sfm.txt"
        )
        self.model_contour = eos.fitting.ModelContour.load(
            models_path + "/sfm_model_contours.json"
        )

    def eos_landmarks(self, landmarks):
        landmarksList = []
        for i, l in enumerate(landmarks, start=1):
            landmarksList.append(
                eos.core.Landmark(str(i), [float(l[0]), float(l[1])])
            )
        return landmarksList

    def fit_face_3d(self, landmarks, image_width, image_height):
        return eos.fitting.fit_shape_and_pose(
            self.morphablemodel_with_expressions,
            self.eos_landmarks(landmarks), self.landmark_mapper,
            image_width, image_height,
            self.edge_topology, self.contour_landmarks,
            self.model_contour, num_iterations=10)

    def generate_mesh(
        self,
        shape_coefficients,
        blendshape_coefficients,
        color_coefficients
    ):
        return self.morphablemodel_with_expressions.draw_sample(
            shape_coefficients, blendshape_coefficients, color_coefficients
        )

    def get_texture(self, mesh, rendering_param, image):
        return eos.render.extract_texture(mesh, rendering_param, image)


def transform_morph_model_to_opengl_data(mesh, rendering_param):
    vertices = [
        val for sublist in np.concatenate(
            (
                transform(
                    mesh,
                    rendering_param.get_modelview(),
                    rendering_param.get_projection()
                ),
                mesh.texcoords
            ),
            axis=1
        )
        for val in sublist.tolist()
    ]

    indices = [val for sublist in mesh.tvi for val in sublist]

    return vertices, indices


def transform(mesh, modelview, projection):
    vertices = np.concatenate(
        (mesh.vertices, np.ones((len(mesh.vertices), 1))), axis=1)
    transformed_vertices = (vertices @ modelview.T @ projection.T)[:, :3]
    z_axis = transformed_vertices[:, 2]
    normalized_z = (z_axis - np.min(z_axis)) / \
        (np.max(z_axis) - np.min(z_axis))
    transformed_vertices[:, 2] = normalized_z
    return transformed_vertices
