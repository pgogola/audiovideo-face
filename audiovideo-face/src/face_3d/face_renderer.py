
from OpenGL.GL import *
from OpenGL.GL.shaders import compileProgram, compileShader
from PIL import Image
from imutils import face_utils
import glfw
import numpy as np
import cv2
import torchvision.transforms as transforms
import torch


class FaceRenderer:
    vertex_src = """
    # version 330
    layout (location = 0) in vec3 aPos;
    layout (location = 1) in vec2 aTexCoords;
    uniform mat4 modelview;
    uniform mat4 projection;
    out vec2 TexCoord;
    void main()
    {
        gl_Position = vec4(aPos, 1.0f);
        // vec4 po = projection * modelview * vec4(aPos, 1.0f);
        // gl_Position = po; //vec4(po.x, po.y, noise1(1), 1.0f);
        TexCoord = aTexCoords;
    }
    """

    fragment_src = """
    # version 330
    in vec2 TexCoord;
    uniform sampler2D ourTexture;
    out vec4 FragColor;
    void main()
    {
        FragColor = texture(ourTexture, TexCoord);
    }
    """

    def __init__(self, teeth_proxy=None):
        self.vertices = None
        self.indices = None
        self.shader = None
        self.texture = None
        self.VBO = None
        self.VAO = None
        self.EBO = None
        self.teeth_proxy = teeth_proxy \
            if teeth_proxy is None \
            else torch.load(teeth_proxy)

        transform_list = []
        osize = [256, 256]
        transform_list.append(transforms.Resize(osize, Image.BICUBIC))
        transform_list += [transforms.ToTensor()]
        transform_list += [transforms.Normalize(
            (0.5, 0.5, 0.5), (0.5, 0.5, 0.5))]
        self.image_transformation = transforms.Compose(transform_list)

    def prepare_shaders(self):
        self.shader = compileProgram(
            compileShader(
                self.vertex_src,
                GL_VERTEX_SHADER
            ),
            compileShader(self.fragment_src, GL_FRAGMENT_SHADER)
        )
        glUseProgram(self.shader)

    def update_model(
        self,
        vertices_textures,
        indices,
        projection=np.eye(4),
        model_view=np.eye(4),
        image=None
    ):
        self.vertices = np.array(vertices_textures, dtype=np.float32)
        self.indices = np.array(indices, dtype=np.uint32)

        glBindVertexArray(self.VAO)
        glBindBuffer(GL_ARRAY_BUFFER, self.VBO)
        glBufferData(GL_ARRAY_BUFFER,
                     self.vertices.nbytes,
                     self.vertices,
                     GL_DYNAMIC_DRAW)
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, self.EBO)
        glBufferData(GL_ELEMENT_ARRAY_BUFFER,
                     self.indices.nbytes,
                     self.indices,
                     GL_DYNAMIC_DRAW)
        glVertexAttribPointer(
            0, 3, GL_FLOAT, GL_FALSE,
            5 * ctypes.sizeof(ctypes.c_float),
            ctypes.c_void_p(0)
        )
        glEnableVertexAttribArray(0)

        glVertexAttribPointer(
            1, 2, GL_FLOAT, GL_FALSE,
            5 * ctypes.sizeof(ctypes.c_float),
            ctypes.c_void_p(3 * ctypes.sizeof(ctypes.c_float))
        )
        glEnableVertexAttribArray(1)

        # binding texture
        glBindTexture(GL_TEXTURE_2D, self.texture)
        # Set the texture wrapping parameters
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT)
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT)
        # Set texture filtering parameters
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR)
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR)

        # load texture convert cv2 image from BGR to RGB (mayby is not necessary)
        image = cv2.cvtColor(image, cv2.COLOR_RGB2BGR)
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, image.shape[0],
                     image.shape[1], 0, GL_RGB, GL_UNSIGNED_BYTE, image)

    def prepare_buffers(self):
        self.VAO = glGenVertexArrays(1)
        self.VBO = glGenBuffers(1)
        self.EBO = glGenBuffers(1)
        self.texture = glGenTextures(1)
        glEnable(GL_DEPTH_TEST)

    def render_face_3d(self):
        glActiveTexture(GL_TEXTURE0)
        glBindTexture(GL_TEXTURE_2D, self.texture)
        glDrawElements(GL_TRIANGLES, len(
            self.indices), GL_UNSIGNED_INT, None)

    def render_face_image(self, width, height, face_landmarks, target_img=None):
        def line_fun(p1, p2):
            xa, ya = p1
            xb, yb = p2
            return lambda x: int((ya - yb) / (xa - xb) * x + (ya - ((ya - yb) / (xa - xb)) * xa))

        data = glReadPixels(
            0, 0, width, height, GL_BGR, GL_UNSIGNED_BYTE)
        renderedImg = np.fromstring(data, dtype=np.uint8)
        renderedImg = renderedImg.reshape(
            (height, width, 3))
        renderedImg = np.flipud(renderedImg)
        if target_img is None:
            return renderedImg

        else:
            gray = cv2.cvtColor(renderedImg, cv2.COLOR_BGR2GRAY)
            mask = np.zeros_like(gray)
            mask[gray != 0] = 255
            inv_mask = np.copy(mask)
            h, w = mask.shape[:2]
            cv2.floodFill(mask, np.zeros(
                (h + 2, w + 2), np.uint8), (0, 0), 255)
            mask = cv2.bitwise_not(mask)
            mask = cv2.bitwise_or(mask, inv_mask)
            mask = cv2.morphologyEx(
                mask, cv2.MORPH_CLOSE,
                np.ones((3, 3), np.uint8)
            )

            bottom_face_mask = np.zeros_like(
                cv2.cvtColor(target_img, cv2.COLOR_BGR2GRAY))
            lf1 = line_fun(face_landmarks[29], face_landmarks[2])
            lf2 = line_fun(face_landmarks[29], face_landmarks[14])

            h, w = bottom_face_mask.shape
            cv2.line(bottom_face_mask, (0, lf1(0)),
                     (face_landmarks[29][0], face_landmarks[29][1]), (255, 255, 255))
            cv2.line(bottom_face_mask, (w, lf2(w)),
                     (face_landmarks[29][0], face_landmarks[29][1]), (255, 255, 255))

            cv2.floodFill(bottom_face_mask, np.zeros(
                (h + 2, w + 2), np.uint8), (face_landmarks[57][0], face_landmarks[57][1]), 255)
            if bottom_face_mask[face_landmarks[57][1], face_landmarks[57][0]] == 0:
                bottom_face_mask = cv2.bitwise_not(bottom_face_mask)

            bottom_face_mask = cv2.GaussianBlur(
                bottom_face_mask, (151, 151), 0)

            landmarks_face_mask = np.zeros_like(bottom_face_mask)
            landmarks_face_mask_convex_hull = cv2.convexHull(
                face_landmarks[0:17])
            cv2.fillConvexPoly(landmarks_face_mask,
                               landmarks_face_mask_convex_hull, 255)
            mask = cv2.bitwise_and(mask, bottom_face_mask)
            # mask = cv2.bitwise_and(mask, landmarks_face_mask)

            chin_mask = np.zeros_like(mask)
            chin_mask_convexhull = cv2.convexHull(face_landmarks[4:13])
            cv2.fillConvexPoly(chin_mask, chin_mask_convexhull, 255)
            chin = cv2.bitwise_and(chin_mask, cv2.bitwise_not(mask))
            mask = cv2.bitwise_or(chin_mask, mask)
            kernel = np.array([[0, 0, 0],
                               [0, 1, 0],
                               [0, 1, 0]], dtype=np.uint8)
            rgb_mask = cv2.cvtColor(mask, cv2.COLOR_GRAY2RGB)
            image_background = (
                target_img * (1.0-(rgb_mask/255))).astype(np.uint8)
            renderedImg = (renderedImg * (rgb_mask/255)).astype(np.uint8)
            result = cv2.add(image_background, renderedImg)
            chin[chin != 255] = 0
            chin_area = np.argwhere(chin != 0)
            if len(chin_area) != 0:
                m_max_x = np.max(chin_area[:, 0])+1
                m_max_y = np.max(chin_area[:, 1])+1
                m_min_x = np.min(chin_area[:, 0])-1
                m_min_y = np.min(chin_area[:, 1])-1
                neck = np.zeros_like(chin)
                neck = cv2.rectangle(
                    neck, (m_min_y, m_min_x), (m_max_y, m_max_x), 255, -1)
                neck = cv2.bitwise_and(result, result, mask=neck)
                dilation = cv2.dilate(neck, kernel, iterations=10)
                dilation = cv2.medianBlur(dilation, 11)
                dilation = cv2.GaussianBlur(dilation, (11, 11), 0)
                dilation = cv2.bitwise_and(dilation, dilation, mask=chin)
                result = cv2.add(result, dilation)

            result = cv2.medianBlur(result, 3)
            return result

    def render_teeth_proxy(self, image, face_detector, mouth_area):
        self.teeth_proxy.eval()
        m_max_x, m_max_y, m_min_x, m_min_y = mouth_area
        mouth_image = image[m_min_x:m_max_x, m_min_y:m_max_y]
        mouth_image = cv2.cvtColor(mouth_image, cv2.COLOR_BGR2RGB)
        mouth_image = Image.fromarray(mouth_image)

        img = self.image_transformation(mouth_image).unsqueeze(0)
        result_teeth = self.teeth_proxy(img)
        image_tensor = result_teeth.data
        image_numpy = image_tensor[0].cpu().float().detach().numpy()
        image_numpy = (np.transpose(image_numpy, (1, 2, 0)) + 1) / 2.0 * 255.0
        image_numpy = image_numpy.astype(np.uint8)
        image_numpy = np.array(image_numpy)[:, :, ::-1]
        image_numpy = cv2.resize(image_numpy,
                                 (m_max_y - m_min_y, m_max_x - m_min_x))
        image[m_min_x:m_max_x, m_min_y:m_max_y] = image_numpy
        return image


def prepare_window(width, height):
    if not glfw.init():
        raise Exception("glfw can not be initialized!")

    glfw.window_hint(glfw.CONTEXT_VERSION_MAJOR, 3)
    glfw.window_hint(glfw.CONTEXT_VERSION_MINOR, 3)
    glfw.window_hint(glfw.OPENGL_FORWARD_COMPAT, GL_TRUE)
    glfw.window_hint(glfw.OPENGL_PROFILE, glfw.OPENGL_CORE_PROFILE)
    # creating the window
    window = glfw.create_window(width, height, "My OpenGL window", None, None)

    # check if window was created
    if not window:
        glfw.terminate()
        raise Exception("glfw window can not be created!")

    # set window's position
    glfw.set_window_pos(window, 400, 200)

    # make the context current
    glfw.make_context_current(window)
    return window
