import pickle
import torch
import utils.logger as log
import cv2
import numpy as np
import glfw
from utils.helpers import *
from face_detection.face_detector import FaceDetector
from face_detection.landmarks_detector import LandmarksDetector
from face_3d.face_renderer import FaceRenderer, prepare_window
from face_3d.morphable_face_model import (
    MorphableFaceModel, transform_morph_model_to_opengl_data
)
from OpenGL.GL import *
from OpenGL.GLU import *


def get_opencv_viewport(width, height):
    return np.array([0, height, width, -height])


def project(obj, model_view, projection, viewport):
    tmp = np.array([obj[0], obj[1], obj[2], 1])
    tmp = model_view @ tmp
    tmp = projection @ tmp
    tmp /= tmp[3]
    tmp = tmp*0.5+0.5
    return np.array(
        [tmp[0]*viewport[2]+viewport[0], tmp[1]*viewport[3]+viewport[1], 1],
        dtype=int
    )


def draw_wireframe(image, mesh, modelview, projection, viewport, color=(0, 255, 0)):
    for triangle in mesh.tvi:
        p1 = project(mesh.vertices[triangle[0]],
                     modelview, projection, viewport)
        p2 = project(mesh.vertices[triangle[1]],
                     modelview, projection, viewport)
        p3 = project(mesh.vertices[triangle[2]],
                     modelview, projection, viewport)
        cv2.line(image, (p1[0], p1[1]), (p2[0], p2[1]), color)
        cv2.line(image, (p2[0], p2[1]), (p3[0], p3[1]), color)
        cv2.line(image, (p3[0], p3[1]), (p1[0], p1[1]), color)


def draw_wireframe_dotted(image, mesh, rendering_param):
    color = (0, 255, 0)
    projection = rendering_param.get_projection()
    projection[3, 3] = 1
    viewport = get_opencv_viewport(image.shape[1], image.shape[0])
    modelview = rendering_param.get_modelview()
    for vertice in mesh.vertices:
        v = project(
            vertice,
            modelview,
            projection,
            viewport
        )
        cv2.circle(image, (v[0], v[1]), 2, (0, 255, 0), -1)


def get_vertex_indices_in_mask(mask_image, mesh, rendering_param, image=None):
    vertices = []
    color = (0, 255, 0)
    projection = rendering_param.get_projection()
    projection[3, 3] = 1
    viewport = get_opencv_viewport(image.shape[1], image.shape[0])
    modelview = rendering_param.get_modelview()
    for index, vertice in enumerate(mesh.vertices):
        v = project(
            vertice,
            modelview,
            projection,
            viewport
        )
        if 255 == mask_image[v[1], v[0]]:
            vertices.append(index)
    if not image is None:
        for index in vertices:
            v = project(
                mesh.vertices[index],
                modelview,
                projection,
                viewport
            )
            cv2.circle(image, (v[0], v[1]), 2, (0, 255, 0), -1)

    print(len(vertices))
    return vertices


def draw_vertices(image, mesh, rendering_param, vertices_upper, vertices_bottom, vertices_mouth):
    vertices = []
    colors = [
        (150, 0, 0),
        (0, 150, 0),
        (0, 0, 150),
    ]
    projection = rendering_param.get_projection()
    projection[3, 3] = 1
    viewport = get_opencv_viewport(image.shape[1], image.shape[0])
    modelview = rendering_param.get_modelview()

    for vertices, color in zip([vertices_upper, vertices_bottom, vertices_mouth], colors):
        for index in vertices:
            v = project(
                mesh.vertices[index],
                modelview,
                projection,
                viewport
            )
            cv2.circle(image, (v[0], v[1]), 1, color, -1)

    print(len(vertices))
    return image


def extract_mouth_mask(image_file):
    face_detector = FaceDetector(gpu=False)
    landmarks_detector = LandmarksDetector()
    face_3d_model = MorphableFaceModel()
    image = cv2.imread(image_file)
    window = prepare_window(
        width=image.shape[1], height=image.shape[0])
    face_renderer = FaceRenderer()
    face_renderer.prepare_shaders()
    face_renderer.prepare_buffers()
    rects = face_detector.get_face_rects(image)
    if 0 == len(rects):
        log.fail(f"No face detected at image {image_file}")
        return

    # prepare area for mouth to mask
    landmark_points = landmarks_detector.get_landmarks(image, rects[0])
    mesh, rendering_param, shape_coefficients, blendshape_coefficients = face_3d_model.fit_face_3d(
        landmark_points, image.shape[1], image.shape[0])
    target_texture = face_3d_model.get_texture(
        mesh, rendering_param, image)
    target_texture = np.transpose(target_texture, (1, 0, 2))
    h, w = image.shape[:2]
    vertices, indices = transform_morph_model_to_opengl_data(
        mesh, rendering_param)
    glfw.poll_events()
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
    face_renderer.update_model(vertices, indices,
                               image=target_texture)
    face_renderer.render_face_3d()
    glfw.swap_buffers(window)
    result = face_renderer.render_face_image(
        w, h, landmark_points, image)

    gray_image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    mouth_mask = np.zeros_like(gray_image)
    mouth_mask_convexhull = cv2.convexHull(
        landmark_points[48:68]
    )
    cv2.fillConvexPoly(mouth_mask, mouth_mask_convexhull, 255)
    mouth_mask = cv2.GaussianBlur(mouth_mask, (61, 61), 0)
    mouth_mask[mouth_mask != 0] = 255
    target_texture = face_3d_model.get_texture(
        mesh, rendering_param, cv2.cvtColor(mouth_mask, cv2.COLOR_GRAY2RGB))
    target_texture = np.transpose(target_texture, (1, 0, 2))
    h, w = image.shape[:2]
    vertices, indices = transform_morph_model_to_opengl_data(
        mesh, rendering_param
    )

    vertices_for_mouth = get_vertex_indices_in_mask(
        mouth_mask,
        mesh,
        rendering_param,
        image
    )

    glfw.poll_events()
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
    face_renderer.update_model(vertices, indices,
                               image=target_texture)
    face_renderer.render_face_3d()
    glfw.swap_buffers(window)
    result = face_renderer.render_face_image(
        w, h, landmark_points, image)

    return vertices_for_mouth


def extract_bottom_face_mask(image_file):
    face_detector = FaceDetector(gpu=False)
    landmarks_detector = LandmarksDetector()
    face_3d_model = MorphableFaceModel()
    image = cv2.imread(image_file)
    window = prepare_window(
        width=image.shape[1], height=image.shape[0])
    face_renderer = FaceRenderer()
    face_renderer.prepare_shaders()
    face_renderer.prepare_buffers()
    rects = face_detector.get_face_rects(image)
    if 0 == len(rects):
        log.fail(f"No face detected at image {image_file}")
        return

    # prepare area for mouth to mask
    landmark_points = landmarks_detector.get_landmarks(image, rects[0])
    mesh, rendering_param, shape_coefficients, blendshape_coefficients = face_3d_model.fit_face_3d(
        landmark_points, image.shape[1], image.shape[0])
    target_texture = face_3d_model.get_texture(
        mesh, rendering_param, image)
    target_texture = np.transpose(target_texture, (1, 0, 2))
    h, w = image.shape[:2]
    vertices, indices = transform_morph_model_to_opengl_data(
        mesh, rendering_param)
    glfw.poll_events()
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
    face_renderer.update_model(vertices, indices,
                               image=target_texture)
    face_renderer.render_face_3d()
    glfw.swap_buffers(window)
    result = face_renderer.render_face_image(
        w, h, landmark_points, image)

    gray_image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    mouth_mask = np.zeros_like(gray_image)
    mouth_mask_convexhull = cv2.convexHull(
        np.array(
            landmark_points[48:68].tolist(
            ) + [[0, h-1], [w-1, h-1]]
        )
    )
    cv2.fillConvexPoly(mouth_mask, mouth_mask_convexhull, 255)
    mouth_mask = cv2.GaussianBlur(mouth_mask, (161, 61), 0)
    mouth_mask[mouth_mask != 0] = 255
    target_texture = face_3d_model.get_texture(
        mesh, rendering_param, cv2.cvtColor(mouth_mask, cv2.COLOR_GRAY2RGB))
    target_texture = np.transpose(target_texture, (1, 0, 2))
    h, w = image.shape[:2]
    vertices, indices = transform_morph_model_to_opengl_data(
        mesh, rendering_param
    )

    vertices_for_mouth = get_vertex_indices_in_mask(
        mouth_mask,
        mesh,
        rendering_param,
        image
    )

    glfw.poll_events()
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
    face_renderer.update_model(vertices, indices,
                               image=target_texture)
    face_renderer.render_face_3d()
    glfw.swap_buffers(window)
    result = face_renderer.render_face_image(
        w, h, landmark_points, image)

    return vertices_for_mouth


def extract_upper_face_mask(image_file):
    face_detector = FaceDetector(gpu=False)
    landmarks_detector = LandmarksDetector()
    face_3d_model = MorphableFaceModel()
    image = cv2.imread(image_file)
    window = prepare_window(
        width=image.shape[1], height=image.shape[0])
    face_renderer = FaceRenderer()
    face_renderer.prepare_shaders()
    face_renderer.prepare_buffers()
    rects = face_detector.get_face_rects(image)
    if 0 == len(rects):
        log.fail(f"No face detected at image {image_file}")
        return

    # prepare area for mouth to mask
    landmark_points = landmarks_detector.get_landmarks(image, rects[0])
    mesh, rendering_param, shape_coefficients, blendshape_coefficients = face_3d_model.fit_face_3d(
        landmark_points, image.shape[1], image.shape[0])
    target_texture = face_3d_model.get_texture(
        mesh, rendering_param, image)
    target_texture = np.transpose(target_texture, (1, 0, 2))
    h, w = image.shape[:2]
    vertices, indices = transform_morph_model_to_opengl_data(
        mesh, rendering_param)
    glfw.poll_events()
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
    face_renderer.update_model(vertices, indices,
                               image=target_texture)
    face_renderer.render_face_3d()
    glfw.swap_buffers(window)
    result = face_renderer.render_face_image(
        w, h, landmark_points, image)

    gray_image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    mouth_mask = np.zeros_like(gray_image)
    mouth_mask_convexhull = cv2.convexHull(
        np.array(
            landmark_points[17:30].tolist(
            ) + landmark_points[[36, 45]].tolist()
        )
    )
    cv2.fillConvexPoly(mouth_mask, mouth_mask_convexhull, 255)
    mouth_mask = cv2.GaussianBlur(mouth_mask, (91, 41), 0)
    mouth_mask[mouth_mask != 0] = 255
    target_texture = face_3d_model.get_texture(
        mesh, rendering_param, cv2.cvtColor(mouth_mask, cv2.COLOR_GRAY2RGB))
    target_texture = np.transpose(target_texture, (1, 0, 2))
    h, w = image.shape[:2]
    vertices, indices = transform_morph_model_to_opengl_data(
        mesh, rendering_param
    )

    vertices_for_mouth = get_vertex_indices_in_mask(
        mouth_mask,
        mesh,
        rendering_param,
        image
    )

    glfw.poll_events()
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
    face_renderer.update_model(vertices, indices,
                               image=target_texture)
    face_renderer.render_face_3d()
    glfw.swap_buffers(window)
    result = face_renderer.render_face_image(
        w, h, landmark_points, image)

    return vertices_for_mouth


def draw_face(image_file, vertices_upper, vertices_bottom, vertices_mouth):
    face_detector = FaceDetector(gpu=False)
    landmarks_detector = LandmarksDetector()
    face_3d_model = MorphableFaceModel()
    image = cv2.imread(image_file)
    window = prepare_window(
        width=image.shape[1], height=image.shape[0])
    face_renderer = FaceRenderer()
    face_renderer.prepare_shaders()
    face_renderer.prepare_buffers()
    rects = face_detector.get_face_rects(image)
    if 0 == len(rects):
        log.fail(f"No face detected at image {image_file}")
        return

    # prepare area for mouth to mask
    landmark_points = landmarks_detector.get_landmarks(image, rects[0])
    mesh, rendering_param, shape_coefficients, blendshape_coefficients = face_3d_model.fit_face_3d(
        landmark_points, image.shape[1], image.shape[0])
    target_texture = face_3d_model.get_texture(
        mesh, rendering_param, image)
    vertices_for_mouth = draw_vertices(
        image,
        mesh,
        rendering_param,
        vertices_upper,
        vertices_bottom,
        vertices_mouth
    )
    return image


if __name__ == "__main__":
    image = "./audiovideo-face/data_additional/image_for_mask.jpg"
    vertices_mouth = extract_mouth_mask(image)
    with open("./audiovideo-face/models/face_3d/mouth_mask.pkl", "wb") \
            as mask_file:
        pickle.dump(vertices_mouth, mask_file)
    vertices_upper = extract_upper_face_mask(image)
    with open("./audiovideo-face/models/face_3d/upper_face_mask.pkl", "wb") \
            as mask_file:
        pickle.dump(vertices_upper, mask_file)
    vertices_bottom = extract_bottom_face_mask(image)
    with open("./audiovideo-face/models/face_3d/bottom_face_mask.pkl", "wb") \
            as mask_file:
        pickle.dump(vertices_bottom, mask_file)
