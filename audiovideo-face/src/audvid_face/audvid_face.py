from __future__ import print_function, division
from torch import nn, optim
import random
import torch
import numpy as np
import os
import eos
import torch.nn.functional as F
import numpy as np
from torch.utils.data import Dataset, DataLoader
from torchvision import transforms, utils
from torch.nn import init
from torch.utils.tensorboard import SummaryWriter
import pickle
from torch.utils.data.sampler import WeightedRandomSampler
import time


def prepare_data_audio_video(
    audio_logits,
    face_coefficients,
    video_fps,
    audio_fps,
    audio_width=16,
    seq_len=8
):
    data_logits = []
    data_coefficients = []
    stride = audio_fps / video_fps
    start = 0
    window_middle = audio_width//2
    audio_logits_length = len(audio_logits)
    for (i, coef) in enumerate(face_coefficients):
        start = int(stride * i) + window_middle
        if face_coefficients[i][1] == []:
            continue
        logits = audio_logits[
            max(0, start - window_middle):
            min(start + window_middle, audio_logits_length)
        ]
        if len(logits) < audio_width:
            continue
        data_logits.append(logits)
        data_coefficients.append(
            np.array(face_coefficients[i][0]+face_coefficients[i][1])
        )
    return np.stack(data_logits), np.stack(data_coefficients)


def prepare_data_audio_to_prediction(
    audio_logits,
    video_fps,
    audio_fps,
    audio_width=16,
    seq_len=8
):
    data_logits = []
    stride = audio_fps / video_fps
    window_middle = audio_width//2
    audio_logits_length = len(audio_logits)
    i = 0
    while True:
        start = int(stride * i) + window_middle
        if start > audio_logits_length:
            break
        logits = audio_logits[
            max(0, start - window_middle):
            min(start + window_middle, audio_logits_length)
        ]
        if len(logits) < audio_width:
            i += 1
            continue
        data_logits.append(logits)
        i += 1
    return np.stack(data_logits)


def prepare_logits_to_prediction(logits):
    logits = torch.FloatTensor(logits)
    all_logits = []
    seq_len = 8
    r = seq_len//2
    for idx, logit in enumerate(logits):
        seq = []
        for i in range(seq_len-r-1, 0, -1):
            j = max(0, idx-i)
            seq.append(logits[j])
        seq.append(logits[idx])
        for i in range(1, seq_len-r+1):
            j = min(len(logits)-1, idx+i)
            seq.append(logits[j])
        all_logits.append(torch.stack(seq))
    return torch.stack(all_logits)


class AudVidFaceDataset(Dataset):
    def __init__(self, logits, face_coefficients, transform=None):
        self.logits, self.face_coefficients, self.indices = \
            self.prepare_data(logits, face_coefficients)
        self.transform = transform
        self.seq_len = 8

    def prepare_logits(self, idx):
        idx = max(0, idx)
        idx = min(idx, len(self.logits)-1)
        seq = []
        r = self.seq_len//2
        for i in range(self.seq_len-r-1, 0, -1):
            j = max(0, idx-i)
            seq.append(self.logits[j])
        seq.append(self.logits[idx])
        for i in range(1, self.seq_len-r+1):
            j = min(len(self.logits)-1, idx+i)
            seq.append(self.logits[j])

        return torch.stack(seq)

    def prepare_coef(self, idx):
        idx = max(0, idx)
        idx = min(idx, len(self.face_coefficients)-1)
        return self.face_coefficients[idx]

    def prepare_data(self, logits, coefficients):
        indices = [i for i in range(0, len(coefficients))]
        coeff = coefficients
        return torch.FloatTensor(logits), torch.FloatTensor(coeff), indices

    def __len__(self):
        return len(self.indices)

    def __getitem__(self, idx):
        if torch.is_tensor(idx):
            idx = idx.tolist()

        current_logits = self.prepare_logits(idx)
        prev_logits = self.prepare_logits(idx-1)
        next_logits = self.prepare_logits(idx+1)

        current_coef = self.prepare_coef(idx)
        prev_coef = self.prepare_coef(idx-1)
        next_coef = self.prepare_coef(idx+1)

        return (
            (current_logits, current_coef),
            (prev_logits, prev_coef),
            (next_logits, next_coef)
        )


class AudVid(nn.Module):
    def __init__(self):
        super(AudVid, self).__init__()
        self.subspace_size = 32
        negative_slope = 0.02
        self.audvid_conv_internal = nn.Sequential(
            nn.Conv2d(in_channels=29, out_channels=32, kernel_size=(3, 1),
                      stride=(2, 1), padding=(1, 0), bias=True),
            nn.LeakyReLU(negative_slope),
            nn.Conv2d(in_channels=32, out_channels=32, kernel_size=(3, 1),
                      stride=(2, 1), padding=(1, 0), bias=True),
            nn.LeakyReLU(negative_slope),
            nn.Conv2d(in_channels=32, out_channels=64, kernel_size=(3, 1),
                      stride=(2, 1), padding=(1, 0), bias=True),
            nn.LeakyReLU(negative_slope),
            nn.Conv2d(in_channels=64, out_channels=64, kernel_size=(3, 1),
                      stride=(2, 1), padding=(1, 0), bias=True),
            nn.LeakyReLU(negative_slope)
        )

        self.audvid_fc_internal = nn.Sequential(
            nn.Linear(in_features=64, out_features=128, bias=True),
            nn.LeakyReLU(negative_slope),
            nn.Linear(in_features=128, out_features=64, bias=True),
            nn.LeakyReLU(negative_slope),
            nn.Linear(in_features=64,
                      out_features=self.subspace_size, bias=True),
            nn.Tanh()
        )

        self.audvid_fc_attention = nn.Sequential(
            nn.Linear(in_features=8, out_features=8, bias=True),
            nn.Softmax(dim=1)
        )

        self.audvid_conv = nn.Sequential(
            nn.Conv1d(in_channels=self.subspace_size, out_channels=16,
                      kernel_size=3, stride=1, padding=1, bias=True),
            nn.LeakyReLU(negative_slope),
            nn.Conv1d(in_channels=16, out_channels=8, kernel_size=3,
                      stride=1, padding=1, bias=True),
            nn.LeakyReLU(negative_slope),
            nn.Conv1d(in_channels=8, out_channels=4, kernel_size=3,
                      stride=1, padding=1, bias=True),
            nn.LeakyReLU(negative_slope),
            nn.Conv1d(in_channels=4, out_channels=2, kernel_size=3,
                      stride=1, padding=1, bias=True),
            nn.LeakyReLU(negative_slope),
            nn.Conv1d(in_channels=2, out_channels=1, kernel_size=3,
                      stride=1, padding=1, bias=True),
            nn.LeakyReLU(negative_slope)
        )

        self.audvid_fc = nn.Sequential(
            nn.Linear(in_features=self.subspace_size,
                      out_features=69, bias=False),
            nn.LeakyReLU(negative_slope)
        )

    def forward_internal(self, x):  # b x seq_len x 16 x 29
        batches = x.shape[0]
        seq_len = x.shape[1]
        audio_sequence = x.view(batches * seq_len, 1, 16, 29)
        # b * seq_len x 1 x 16 x 29
        audio_sequence = torch.transpose(audio_sequence, 1, 3)
        # b * seq_len x 29 x 16 x 1
        # print("audio_sequence.shape", audio_sequence.shape)
        internal_conv_res = self.audvid_conv_internal(audio_sequence)
        # b * seq_len x 64 x 1 x 1
        # print("internal_conv_res.shape", internal_conv_res.shape)
        internal_conv_res = torch.reshape(
            internal_conv_res, (batches * seq_len, 1, -1)
        )
        # b * seq_len x 1 x 64
        # print("internal_conv_res.shape", internal_conv_res.shape)
        subspace = self.audvid_fc_internal(internal_conv_res)
        # b * seq_len x 1 x 32
        subspace = subspace[:, 0, :]
        # b * seq_len x 32
        subspace = subspace.view(batches, seq_len, self.subspace_size)
        # b x seq_len x 32
        return subspace

    def forward(self, x):  # b x seq_len x 16 x 29
        batches = x.shape[0]
        seq_len = x.shape[1]
        subspace = self.forward_internal(x)  # b x seq_len x 32

        subspace_T = torch.transpose(subspace, 1, 2)  # b x 32 x seq_len
        expression = subspace_T[:, :, (seq_len // 2):(seq_len // 2) + 1]
        # b x 32 x 1
        filtered_subspace = self.audvid_conv(subspace_T)  # b x 1 x seq_len
        attention = self.audvid_fc_attention(
            filtered_subspace.view(batches, seq_len)
        ).view(batches, seq_len, 1)
        filtered_expression = torch.bmm(subspace_T, attention)
        filtered_expression = self.audvid_fc(filtered_expression[:, :, 0])
        expression = self.audvid_fc(expression[:, :, 0])
        return 10 * filtered_expression, 10 * expression


def init_weights(net, init_type='normal', gain=0.02):
    def init_func(m):
        classname = m.__class__.__name__
        if hasattr(m, 'weight') and (classname.find('Conv') != -1 or classname.find('Linear') != -1):
            if init_type == 'normal':
                init.normal_(m.weight.data, 0.0, gain)
            elif init_type == 'xavier':
                init.xavier_normal_(m.weight.data, gain=gain)
            elif init_type == 'kaiming':
                init.kaiming_normal_(m.weight.data, a=0.02, mode='fan_in')
            elif init_type == 'orthogonal':
                init.orthogonal_(m.weight.data, gain=gain)
            else:
                raise NotImplementedError(
                    'initialization method [%s] is not implemented' % init_type)
            if hasattr(m, 'bias') and m.bias is not None:
                init.constant_(m.bias.data, 0.0)
        elif classname.find('BatchNorm2d') != -1:
            init.normal_(m.weight.data, 1.0, gain)
            init.constant_(m.bias.data, 0.0)

    print('initialize network with %s' % init_type)
    net.apply(init_func)


def init_net(net, init_type='normal', init_gain=0.02, gpu_ids=[]):
    if len(gpu_ids) > 0:
        assert(torch.cuda.is_available())
        net.to(gpu_ids[0])
        # net = torch.nn.DataParallel(net, gpu_ids)
    init_weights(net, init_type, gain=init_gain)
    return net


class RMSELoss(nn.Module):
    def __init__(self):
        super().__init__()
        self.mse = nn.MSELoss()

    def forward(self, yhat, y):
        return torch.sqrt(self.mse(yhat, y))


class MyLoss(nn.Module):
    def __init__(self):
        super().__init__()
        my_path = os.path.abspath(os.path.dirname(__file__))
        models_path = os.path.join(my_path, "../../models/face_3d")
        self.shape = eos.morphablemodel.load_model(
            models_path + "/sfm_shape_3448.bin")
        self.blendshapes = eos.morphablemodel.load_blendshapes(
            models_path + "/expression_blendshapes_3448.bin")
        self.landmark_mapper = eos.core.LandmarkMapper(
            models_path + "/ibug_to_sfm.txt"
        )
        # landmarks_mouth = (48, 61)
        self.shape_mean = self.shape.get_shape_model().get_mean()
        self.mask = np.array([0.1]*len(self.shape_mean))

        with open(models_path + "/upper_face_mask.pkl", "rb") \
                as mask_file:
            vertices = pickle.load(mask_file)
            for i in vertices:
                self.mask[[i*3, i*3+1, i*3+2]] = 0.01

        with open(models_path + "/bottom_face_mask.pkl", "rb") \
                as mask_file:
            vertices = pickle.load(mask_file)
            for i in vertices:
                self.mask[[i*3, i*3+1, i*3+2]] = 1

        with open(models_path + "/mouth_mask.pkl", "rb") \
                as mask_file:
            vertices = pickle.load(mask_file)
            for i in vertices:
                self.mask[[i*3, i*3+1, i*3+2]] = 1.5
        self.mask = torch.from_numpy(
            self.mask
        ).to('cuda')

        self.shape_mean = torch.from_numpy(
            self.shape.get_shape_model().get_mean()
        ).to('cuda')
        self.shape_rescaled_pca_basis = torch.from_numpy(
            self.shape.get_shape_model().get_rescaled_pca_basis()
        ).to('cuda')

        self.expression = torch.from_numpy(
            np.array(
                [deformation.deformation.tolist()
                 for deformation in self.blendshapes]
            ).astype(np.float32)
        ).to('cuda')

        self.rmse1 = RMSELoss()
        self.rmse2 = RMSELoss()

    def rmse(self, val):
        return torch.sqrt(torch.mean(val))

    def abs(self, val):
        return torch.mean(torch.abs(val))

    def coefficients_to_vector_space(self, coefficients):
        shape_pca = coefficients[:, :63]
        expression_pca = coefficients[:, 63:]
        model_sample = self.shape_mean.view(
            1, -1) + (self.shape_rescaled_pca_basis @ shape_pca.T).T
        expression_sample = expression_pca @ self.expression
        return model_sample + expression_sample

    def forward(
        self,
        expression,
        internal_expression,
        target_expression,
        expression_prev,
        target_expression_prev,
        expression_next,
        target_expression_next
    ):
        expression_v = self.coefficients_to_vector_space(
            expression
        )

        internal_expression_v = self.coefficients_to_vector_space(
            internal_expression
        )

        target_expression_v = self.coefficients_to_vector_space(
            target_expression
        )

        expression_prev_v = self.coefficients_to_vector_space(
            expression_prev
        )

        target_expression_prev_v = self.coefficients_to_vector_space(
            target_expression_prev
        )

        expression_next_v = self.coefficients_to_vector_space(
            expression_next
        )

        target_expression_next_v = self.coefficients_to_vector_space(
            target_expression_next
        )

        # diff_current_coef = expression-target_expression
        diff_vertices_current = expression_v - target_expression_v

        # diff_current_expression_internal = internal_expression-target_expression
        diff_vertices_intermediate = internal_expression_v - target_expression_v

        # diff_expression_prev = expression_prev-target_expression_prev
        diff_vertices_prev = expression_prev_v - target_expression_prev_v

        # diff_expression_next = expression_next-target_expression_next
        diff_vertices_next = expression_next_v - target_expression_next_v

        # relative (temporal 1 timestep) cur - nxt and prv - cur
        diff_vertices_tmp_current_next = (expression_v - expression_next_v) - \
            (target_expression_v - target_expression_next_v)

        diff_vertices_tmp_prev_current = (expression_prev_v - expression_v) - \
            (target_expression_prev_v - target_expression_v)

        # relative (temporal 2 timesteps)  nxt - prv
        diff_vertices_tmp_nxt_prv = (expression_next_v - expression_prev_v) - \
            (target_expression_next_v - target_expression_prev_v)

        return \
            100 * self.rmse(
                self.mask*diff_vertices_current*diff_vertices_current
            ) + \
            100 * self.rmse(
                self.mask*diff_vertices_prev*diff_vertices_prev
            ) + \
            100 * self.rmse(
                self.mask*diff_vertices_next*diff_vertices_next
            ) + \
            300 * self.rmse(
                self.mask*diff_vertices_intermediate*diff_vertices_intermediate
            ) + \
            2000 * self.rmse(
                self.mask*diff_vertices_tmp_current_next*diff_vertices_tmp_current_next
            ) + \
            2000 * self.rmse(
                self.mask*diff_vertices_tmp_prev_current*diff_vertices_tmp_prev_current
            ) + \
            2000 * self.rmse(
                self.mask*diff_vertices_tmp_nxt_prv*diff_vertices_tmp_nxt_prv
            )


def train_model(
        audvid_face,
        data,
        batch_size,
        epochs_const_lr,
        epochs_decay_lr,
        device):
    kwargs = {
        'num_workers': 1,
        'pin_memory': True
    } if device == 'cuda' else {
        'num_workers': 4
    }

    part_of_learning_set = 1.0
    audio_logits, coefficients = data
    transformed_dataset = AudVidFaceDataset(
        logits=audio_logits[
            :int(part_of_learning_set*audio_logits.shape[0])
        ],
        face_coefficients=coefficients[
            :int(part_of_learning_set*audio_logits.shape[0])
        ]
    )
    dataloader = DataLoader(
        transformed_dataset,
        batch_size=batch_size,
        shuffle=True,
        **kwargs
    )

    transformed_dataset_val = AudVidFaceDataset(
        logits=audio_logits[
            int((part_of_learning_set)*audio_logits.shape[0]):
        ],
        face_coefficients=coefficients[
            int((part_of_learning_set)*audio_logits.shape[0]):
        ]
    )
    dataloader_val = DataLoader(
        transformed_dataset_val,
        batch_size=batch_size,
        shuffle=True,
        **kwargs
    ) if len(transformed_dataset_val) > 0 else None
    audvid_face.to(device)

    criterion = MyLoss()
    optimizer = optim.Adam(audvid_face.parameters(), lr=10e-4, amsgrad=True)

    def lambda1(epoch):
        if 0 == epochs_decay_lr:
            return 1
        return 1 - max(0, (epoch - epochs_const_lr) / (epochs_decay_lr))

    show_loss_it = 100
    scheduler = optim.lr_scheduler.LambdaLR(optimizer, lr_lambda=lambda1)
    writer = SummaryWriter()
    for epoch in range(epochs_const_lr + epochs_decay_lr):
        audvid_face.train()
        running_loss = 0.0
        for i, (
            (inputs_audio, target_expression),
            (inputs_audio_prev, target_expression_prev),
            (inputs_audio_next, target_expression_next)
        ) in enumerate(dataloader, 0):
            inputs_audio = inputs_audio.to(device)
            target_expression = target_expression.to(device)
            inputs_audio_prev = inputs_audio_prev.to(device)
            target_expression_prev = target_expression_prev.to(device)
            inputs_audio_next = inputs_audio_next.to(device)
            target_expression_next = target_expression_next.to(device)
            optimizer.zero_grad()
            expression, internal_expression = audvid_face(inputs_audio)
            expression_prev, _ = audvid_face(inputs_audio_prev)
            expression_next, _ = audvid_face(inputs_audio_next)
            # print("expression.shape", expression.shape)
            # print("internal_expression.shape", internal_expression.shape)
            # print("labels.shape", labels.shape)
            loss = criterion(
                expression,
                internal_expression,
                target_expression,
                expression_prev,
                target_expression_prev,
                expression_next,
                target_expression_next
            )
            loss.backward()
            optimizer.step()
            running_loss += loss.item()
        audvid_face.eval()
        running_loss_val = 0.0
        if not dataloader_val is None:
            for i, (inputs, labels), _, _ in enumerate(dataloader_val, 0):
                inputs = inputs.to(device)
                labels = labels.to(device)
                optimizer.zero_grad()
                expression, internal_expression = audvid_face(inputs)
                # print("expression.shape", expression.shape)
                # print("internal_expression.shape", internal_expression.shape)
                # print("labels.shape", labels.shape)
                loss1 = criterion1(expression, labels)
                loss2 = criterion2(internal_expression, labels)
                loss = loss1+loss2
                running_loss_val += loss.item()
        print(
            f"Epoch: {epoch+1}/{epochs_const_lr + epochs_decay_lr}\t" +
            f"Train dataset loss: {running_loss/len(dataloader):.5f}\t" +
            "Validation dataset loss: " +
            (
                "N/A"
                if dataloader_val is None
                else f"{running_loss_val / len(dataloader_val):.5f}"
            )
        )
        writer.add_scalar("loss x epoch", running_loss/len(dataloader), epoch)
        scheduler.step()
    writer.close()
    return audvid_face


def save_model(model, file_name):
    torch.save(model, file_name)


def load_model(file_name):
    model = AudVid()
    model = torch.load(file_name)
    model.eval()
    return model


if __name__ == "__main__":
    device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
    print(device)

    files_trump = [
        "./trump_tmp/pVXSAYqbVjW2n8iN_1595902684.0875313.pickle"
    ]
    files_obama = [
        "./obama_tmp/2VEbbuN1A1I6jcXl_1595913314.1080534.pickle"
    ]
    files_duda = [
        "./duda_tmp/RDYt6HFbvMZycLPJ_1595974054.5462906.pickle",
        "./duda_tmp/alz36Jsge16adTBb_1595972576.5055594.pickle",
        "./duda_tmp/jp3INwFZ3o86LYJQ_1595971768.7137077.pickle",
        "./duda_tmp/CHpzm8PonNf81xll_1595970931.0146506.pickle",
        "./duda_tmp/fm9O4i5S2QiV2MQE_1595970107.5576313.pickle"
    ]
    files_train = [
        files_trump,
        files_obama,
        files_duda
    ]

    model_names = [
        "./audiovideo-face/models/audvid/audvid_trump_model_",
        "./audiovideo-face/models/audvid/audvid_obama_model_",
        "./audiovideo-face/models/audvid/audvid_duda_model_",
    ]

    fpses = [
        30,
        30,
        25
    ]

    for actor in [
        0,
        1,
        2
    ]:
        start_t = time.time()
        data_logits = []
        data_coef = []
        co = []
        for sv in files_train[actor]:
            with open(sv, 'rb') as f:
                audio_logits, face_frames_coefficients = pickle.load(f)
            for c in face_frames_coefficients:
                if len(c[0]) > 0:
                    co.append(c[0])

            logits, coef = prepare_data_audio_video(
                audio_logits=audio_logits,
                face_coefficients=face_frames_coefficients,
                video_fps=fpses[actor],
                audio_fps=50,
                audio_width=16,
                seq_len=8
            )

            data_logits.append(logits)
            data_coef.append(coef)

        data_logits = np.concatenate(data_logits, axis=0)
        data_coef = np.concatenate(data_coef, axis=0)

        audvid_face = AudVid()
        init_net(audvid_face, init_type='xavier')
        device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
        audvid_face.train()
        audvid_face = train_model(
            audvid_face=audvid_face,
            data=(data_logits, data_coef),
            batch_size=16,
            epochs_const_lr=50,
            epochs_decay_lr=50,
            device=device
        )
        stop_t = time.time()
        print("time: ", time.strftime("%H:%M:%S", time.gmtime(stop_t-start_t)))
        save_model(
            audvid_face, model_names[actor]+".pth")
        # audvid_face = AudVid()
        # init_net(audvid_face, init_type='xavier')
        # device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
        # audvid_face.train()
        # data_logits = np.concatenate(
        #     [
        #         data_logits,
        #         data_logits+np.random.normal(0, 1, (data_logits.shape)),
        #         data_logits+np.random.normal(0, 1, (data_logits.shape))
        #     ]
        # )
        # data_coef = np.concatenate(
        #     [
        #         data_coef,
        #         data_coef+np.random.normal(0, 0.05, (data_coef.shape)),
        #         data_coef+np.random.normal(0, 0.05, (data_coef.shape))
        #     ]
        # )
        # audvid_face = train_model(
        #     audvid_face=audvid_face,
        #     data=(data_logits, data_coef),
        #     batch_size=16,
        #     epochs_const_lr=20,
        #     epochs_decay_lr=30,
        #     device=device
        # )
        # save_model(
        #     audvid_face, model_names[actor]+"_face.pth")
