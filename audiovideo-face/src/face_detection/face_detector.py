import os.path
import dlib
import cv2
import numpy as np


class FaceDetector(object):
    def __init__(self, padding=(0, 0, 0, 0), gpu=True):
        my_path = os.path.abspath(os.path.dirname(__file__))
        models_path = os.path.join(my_path, "../../models/face_detector")
        self.gpu = gpu
        if gpu:
            self.detector = dlib.cnn_face_detection_model_v1(
                models_path + "/mmod_human_face_detector.dat"
            )
        else:
            DNN = "TF"
            if DNN == "CAFFE":
                modelFile = models_path + "/res10_300x300_ssd_iter_140000_fp16.caffemodel"
                configFile = models_path + "/deploy.prototxt"
                self.detector = cv2.dnn.readNetFromCaffe(configFile, modelFile)
            else:
                modelFile = models_path + "/opencv_face_detector_uint8.pb"
                configFile = models_path + "/opencv_face_detector.pbtxt"
                self.detector = cv2.dnn.readNetFromTensorflow(
                    modelFile, configFile)

    def get_face_rects(self, image):
        """
        image : grayscale image to detect faces
        """
        if self.gpu:
            bboxes = self.detector(image, 0)
        else:
            conf_threshold = 0.9
            blob = cv2.dnn.blobFromImage(image, 1.0, (300, 300), [
                104, 117, 123], False, False)
            self.detector.setInput(blob)
            detections = self.detector.forward()
            bboxes = []
            (h, w) = image.shape[:2]
            for i in range(detections.shape[2]):
                confidence = detections[0, 0, i, 2]
                if confidence > conf_threshold:
                    box = detections[0, 0, i, 3:7] * np.array([w, h, w, h])
                    (startX, startY, endX, endY) = box.astype("int")
                    bboxes.append(dlib.rectangle(startX, startY, endX, endY))

        return bboxes
