from imutils import face_utils
import numpy as np
import os
import dlib
import cv2


class LandmarksDetector(object):
    def __init__(self):
        my_path = os.path.abspath(os.path.dirname(__file__))
        models_path = os.path.join(my_path, "../../models/landmarks_detector")
        self.model_file = models_path + "/shape_predictor_68_face_landmarks.dat"
        self.predictor = dlib.shape_predictor(self.model_file)

    def get_landmarks(self, image, face_rect):
        """
        image : grayscale image to find landmarks
        face_rect : face rectangle to find landmarks within
        """
        landmarks = self.predictor(image, face_rect)
        landmarks = face_utils.shape_to_np(landmarks)
        return landmarks


def draw_landmarks(image, landmarks):
    for (x, y) in landmarks:
        cv2.circle(image, (x, y), 2, (0, 255, 0), -1)
    return image


def draw_landmark_based_contours(
    image,
    landmarks,
    color=(255, 255, 255),
    thickness=1
):
    def reshape_for_polyline(array):
        """Reshape image so that it works with polyline."""
        return np.array(array, np.int32).reshape((-1, 1, 2))
    jaw = reshape_for_polyline(landmarks[0:17])
    left_eyebrow = reshape_for_polyline(landmarks[22:27])
    right_eyebrow = reshape_for_polyline(landmarks[17:22])
    nose_bridge = reshape_for_polyline(landmarks[27:31])
    lower_nose = reshape_for_polyline(landmarks[30:35])
    left_eye = reshape_for_polyline(landmarks[42:48])
    right_eye = reshape_for_polyline(landmarks[36:42])
    outer_lip = reshape_for_polyline(landmarks[48:60])
    inner_lip = reshape_for_polyline(landmarks[60:68])

    cv2.polylines(image, [jaw], False, color, thickness)
    cv2.polylines(image, [left_eyebrow], False, color, thickness)
    cv2.polylines(image, [right_eyebrow], False, color, thickness)
    cv2.polylines(image, [nose_bridge], False, color, thickness)
    cv2.polylines(image, [lower_nose], True, color, thickness)
    cv2.polylines(image, [left_eye], True, color, thickness)
    cv2.polylines(image, [right_eye], True, color, thickness)
    cv2.polylines(image, [outer_lip], True, color, thickness)
    cv2.polylines(image, [inner_lip], True, color, thickness)
    return image
