import torchvision.transforms as transforms
from PIL import Image
import torch
import utils.logger as log
import cv2
import numpy as np
import glfw
import math
import argparse
import os
import subprocess
import string
import time
import pickle
import random
from pathlib import Path
from utils.audio import extract_audio
from utils.video import get_video_details
from utils.helpers import *
from utils.time_count import *
from deep_speech.deep_speech import DeepSpeech
from face_detection.face_detector import FaceDetector
from face_detection.landmarks_detector import LandmarksDetector
from face_3d.face_renderer import FaceRenderer, prepare_window
from face_3d.morphable_face_model import (
    MorphableFaceModel, transform_morph_model_to_opengl_data
)
import audvid_face.audvid_face as avf
from OpenGL.GL import *
from OpenGL.GLU import *
from imutils import face_utils
from scipy.special import softmax
from sklearn.preprocessing import StandardScaler


def extract_visual_face_coefficients(video_file,
                                     face_detector,
                                     landmarks_detector,
                                     face_3d_model,
                                     teeth_directories,
                                     opts):

    coef = []
    cap = cv2.VideoCapture(video_file)
    total_frames_num = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))
    i = 0
    _, image = cap.read()
    window = prepare_window(
        width=image.shape[1], height=image.shape[0])
    cap.set(cv2.CAP_PROP_POS_FRAMES, 0)

    face_renderer = FaceRenderer()
    face_renderer.prepare_shaders()
    face_renderer.prepare_buffers()
    num = 0

    train_threshold_num = opts.tproxy_train
    val_threshold_num = train_threshold_num + opts.tproxy_val
    test_threshold_num = val_threshold_num + opts.tproxy_test

    while True:
        if num % 100 == 0:
            log.log_info(f"Face coefficients: {num}/{total_frames_num}")
        num = num + 1
        retval, image = cap.read()
        if retval is False:
            break
        rects = face_detector.get_face_rects(image)
        if 0 != len(rects):
            landmark_points = landmarks_detector.get_landmarks(image, rects[0])
            mesh, rendering_param, shape_coefficients, blendshape_coefficients = face_3d_model.fit_face_3d(
                landmark_points, image.shape[1], image.shape[0])
            coef.append((shape_coefficients,
                         blendshape_coefficients))
            if num < test_threshold_num:
                if num < train_threshold_num:
                    A_directory = teeth_directories["trainA"]
                    B_directory = teeth_directories["trainB"]
                elif num < val_threshold_num:
                    A_directory = teeth_directories["valA"]
                    B_directory = teeth_directories["valB"]
                elif num < test_threshold_num:
                    A_directory = teeth_directories["testA"]
                    B_directory = teeth_directories["testB"]
                target_texture = face_3d_model.get_texture(
                    mesh, rendering_param, image)
                target_texture = np.transpose(target_texture, (1, 0, 2))
                h, w = image.shape[:2]
                target_texture_mask = np.ones((h, w), dtype=np.uint8)*255
                vertices, indices = transform_morph_model_to_opengl_data(
                    mesh, rendering_param)
                glfw.poll_events()
                glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
                face_renderer.update_model(vertices, indices,
                                           image=target_texture_mask)
                face_renderer.render_face_3d()
                glfw.swap_buffers(window)
                result_mask = cv2.cvtColor(face_renderer.render_face_image(
                    w, h, landmark_points, None), cv2.COLOR_RGB2GRAY)
                cv2.floodFill(result_mask, np.zeros(
                    (h + 2, w + 2), np.uint8), (0, 0), 255)
                mouth_area = np.argwhere(result_mask == 0)
                m_max_x = np.max(mouth_area[:, 0])+1
                m_max_y = np.max(mouth_area[:, 1])+1
                m_min_x = np.min(mouth_area[:, 0])-1
                m_min_y = np.min(mouth_area[:, 1])-1
                max_width = max(m_max_x-m_min_x, m_max_y-m_min_y)
                middle_x = (m_max_x+m_min_x)//2
                middle_y = (m_max_y+m_min_y)//2
                m_max_x = middle_x + max_width//2
                m_max_y = middle_y + max_width//2
                m_min_x = middle_x - max_width//2
                m_min_y = middle_y - max_width//2
                time.sleep(0.01)
                glfw.poll_events()
                glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
                face_renderer.update_model(vertices, indices,
                                           image=target_texture)
                face_renderer.render_face_3d()
                glfw.swap_buffers(window)
                result = face_renderer.render_face_image(
                    w, h, landmark_points, None)
                random_name = ''.join(random.choices(
                    string.ascii_letters + string.digits, k=16))
                cv2.imwrite(B_directory + "/img" + random_name + ".jpg",
                            image[m_min_x:m_max_x, m_min_y:m_max_y])
                cv2.imwrite(A_directory + "/img" + random_name + ".jpg",
                            result[m_min_x:m_max_x, m_min_y:m_max_y])
            else:
                exit()
        else:
            coef.append(([], []))
    glfw.terminate()
    return coef


def merge_audio_visual_train_data(audio_logits, face_coefficients, video_fps, audio_stride=10, audio_width=16):
    data = []
    video_stride = 1000 / video_fps
    for (i, coef) in enumerate(face_coefficients):
        if 0 == len(coef):
            continue
        start = int(video_stride * i / audio_stride)
        logits = audio_logits[start:start + audio_width]
        if len(logits) < audio_width:
            continue
        data.append((np.expand_dims(logits, axis=0), coef))
    return data


def prepare_video_train_data(
    video_file,
    deepspeech_model,
    face_detector,
    landmarks_detector,
    face_3d_model,
    teeth_directories,
    opts
):

    filename, file_extension = os.path.splitext(video_file)
    audio_tmp_file = os.path.join(
        opts.tmp_dir, filename.split("/")[-1] + ".wav")
    extract_audio(video_file, audio_tmp_file, bitrate=3000, fps=16000)
    video_fps, frames_count = get_video_details(video_file)
    log.log_info("Generating audio logits")
    audio_logits = deepspeech_model.run_inference(audio_tmp_file)
    audio_logits = softmax(audio_logits)
    audio_logits = StandardScaler().fit_transform(audio_logits)*10
    log.log_info("Generating face coefficients")
    face_frames_coefficients = extract_visual_face_coefficients(
        video_file,
        face_detector,
        landmarks_detector,
        face_3d_model,
        teeth_directories,
        opts
    )
    random_name = ''.join(random.choices(
        string.ascii_letters + string.digits, k=16))
    with open(f"{opts.tmp_dir}/{random_name}_{get_time()}.pickle", "wb") as of:
        pickle.dump((audio_logits, face_frames_coefficients), of)
    return avf.prepare_data_audio_video(
        audio_logits=audio_logits,
        face_coefficients=face_frames_coefficients,
        video_fps=video_fps,
        audio_fps=50,
        audio_width=16
    )


def train(source_videos, opts, teeth_directories):
    deep_speech = DeepSpeech()
    deep_speech.load_model()
    face_detector = FaceDetector(gpu=False)
    landmarks_detector = LandmarksDetector()
    face_3d_model = MorphableFaceModel()
    data_logits = []
    data_coef = []

    data_prepare_start_time = get_time()
    for sv in source_videos:
        logits, coef = prepare_video_train_data(
            sv,
            deep_speech,
            face_detector,
            landmarks_detector,
            face_3d_model,
            teeth_directories,
            opts
        )
        data_logits.append(logits)
        data_coef.append(coef)

    data_prepare_stop_time = get_time()
    log.log_info(
        "Prepare data elapsed time: " +
        get_elapsed_time(data_prepare_start_time,
                         data_prepare_stop_time)
    )

    data_logits = np.concatenate(data_logits, axis=0)
    data_coef = np.concatenate(data_coef, axis=0)

    audvid_face = avf.AudVid()
    device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
    avf.init_net(audvid_face, init_type='xavier')
    audvid_face.train()
    train_audvid_model_start = get_time()
    avf.train_model(
        audvid_face=audvid_face,
        data=(data_logits, data_coef),
        batch_size=args.audvid_batch,
        epochs_const_lr=args.audvid_epochs,
        epochs_decay_lr=args.audvid_epochs_decay,
        device=device
    )
    train_audvid_model_stop = get_time()

    log.log_info(
        "Learn model AudVid elapsed time: " +
        get_elapsed_time(train_audvid_model_start,
                         train_audvid_model_stop)
    )
    # audvid_face.to("cpu")
    torch.cuda.empty_cache()
    return audvid_face


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        formatter_class=argparse.RawTextHelpFormatter)
    required_args = parser.add_argument_group('required arguments')
    required_args.add_argument("--movies-dir", type=str, dest="movies_dir",
                               action="store", required=True,
                               help="Directory with movies to train system.")
    required_args.add_argument("--audvid-model", type=str, dest="audvid_model",
                               action="store", required=True,
                               help="AudVid model output name.")
    # required_args.add_argument("--teeth-proxy-model", type=str, dest="teeth_proxy_model",
    #                            action="store", required=True,
    #                            help="Teeth proxy model output name.")
    required_args.add_argument("--tmp-dir", type=str, dest="tmp_dir",
                               action="store", required=False, default="./tmp",
                               help="Temporary work directory.")

    audvid_args = parser.add_argument_group('AudVid arguments')
    audvid_args.add_argument("--audvid-epochs", type=int, dest="audvid_epochs",
                             action="store", required=False, default=200,
                             help="AudVidFace model train epochs with const initial learning rate.")
    audvid_args.add_argument("--audvid-epochs-decay", type=int, dest="audvid_epochs_decay",
                             action="store", required=False, default=200,
                             help="AudVidFace model train epochs witch decaing learning rate.")
    audvid_args.add_argument("--audvid-batch", type=int, dest="audvid_batch",
                             action="store", required=False, default=50,
                             help="AudVid model train batch size.")

    teeth_proxy_args = parser.add_argument_group(
        'TeethProxy arguments - now run separated proces'
    )
    teeth_proxy_args.add_argument("--tproxy-train", type=int, dest="tproxy_train",
                                  action="store", required=False, default=1000,
                                  help="Teeth proxy cGAN model train samples number.")
    teeth_proxy_args.add_argument("--tproxy-test", type=int, dest="tproxy_test",
                                  action="store", required=False, default=500,
                                  help="Teeth proxy cGAN model test samples number.")
    teeth_proxy_args.add_argument("--tproxy-val", type=int, dest="tproxy_val",
                                  action="store", required=False, default=500,
                                  help="Teeth proxy cGAN model val samples number.")
    # teeth_proxy_args.add_argument('--tproxy-epochs', type=int, dest="tp_n_epochs",
    #                               action="store", required=False, default=20,
    #                               help='number of epochs with the initial learning rate')
    # teeth_proxy_args.add_argument('--tproxy-epochs-decay', type=int, dest="tp_n_epochs_decay",
    #                               action="store", required=False, default=20,
    #                               help='number of epochs to linearly decay learning rate to zero')

    args = parser.parse_args()

    teeth_partA = "/teeth/A"
    teeth_partB = "/teeth/B"
    teeth_directories = {
        "trainA": args.tmp_dir + teeth_partA + "/train",
        "testA": args.tmp_dir + teeth_partA + "/test",
        "valA": args.tmp_dir + teeth_partA + "/val",
        "trainB": args.tmp_dir + teeth_partB + "/train",
        "testB": args.tmp_dir + teeth_partB + "/test",
        "valB": args.tmp_dir + teeth_partB + "/val"
    }
    result_teeth_dir = args.tmp_dir + "/data"

    for td in teeth_directories.values():
        tmp_dir_path = Path(td)
        tmp_dir_path.mkdir(parents=True, exist_ok=True)

    train_videos = [
        os.path.join(args.movies_dir, f) for f in os.listdir(args.movies_dir)
    ]
    log.log_green("Found videos:\n" + str(os.listdir(args.movies_dir)))

    model = train(
        train_videos,
        args,
        teeth_directories
    )
    log.log_green("Saving AudVid model")
    avf.save_model(model, args.audvid_model)

    cmd_run_prepare_teeth_proxy_data = [
        "python",
        "./audiovideo-face/src/utils/combine_A_and_B.py",
        "--fold_A",  args.tmp_dir + teeth_partA,
        "--fold_B",  args.tmp_dir + teeth_partB,
        "--fold_AB", result_teeth_dir
    ]

    prepare_data_for_pix2pix_start = get_time()
    log.log_green(
        "Calling subrocess:\n" + str(cmd_run_prepare_teeth_proxy_data))
    subprocess.Popen(cmd_run_prepare_teeth_proxy_data).wait()
    prepare_data_for_pix2pix_stop = get_time()

    log.log_info(
        "Prepare data for pix2pix elapsed time: " +
        get_elapsed_time(prepare_data_for_pix2pix_start,
                         prepare_data_for_pix2pix_stop)
    )

    # cmd_run_train_teeth_proxy = [
    #     "python",
    #     "./pytorch-CycleGAN-and-pix2pix/train.py",
    #     "--dataroot", result_teeth_dir,
    #     "--name", "teeth_pix2pix",
    #     "--model", "pix2pix",
    #     "--direction", "AtoB",
    #     "--save_by_iter",
    #     "--save_epoch_freq", "1",
    #     "--lambda_L1", "10",
    #     "--dataset_mode", "aligned",
    #     "--norm", "batch",
    #     "--pool_size", "0",
    #     "--n_epochs", str(args.tp_n_epochs),
    #     "--n_epochs_decay", str(args.tp_n_epochs_decay),
    #     "--teeth-proxy-model", args.teeth_proxy_model
    # ]

    # learn_pix2pix_start = get_time()
    # log.log_green(
    #     "Calling subrocess:\n" + str(cmd_run_train_teeth_proxy))
    # subprocess.Popen(cmd_run_train_teeth_proxy).wait()
    # learn_pix2pix_stop = get_time()

    # log.log_info(
    #     "Learn pix2pix teeth proxy elapsed time: " +
    #     get_elapsed_time(learn_pix2pix_start,
    #                      learn_pix2pix_stop)
    # )
